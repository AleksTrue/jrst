#!/usr/bin/python
# coding=utf-8

import __builtin__, os, sys, string, time
from java.io import File, InputStream
from java.net import URL, JarURLConnection

def init_docutils(docutilsPath):
    # Check if the new open function doesn't exist
    if "openlegacy" not in __builtin__.__dict__:
        
        # Define the function jaropen which will be able to read into the jar.
        # Examples : 
        # If the value of the resourcePath is 
        # '/home/user/workspace/jrst/src/test/resources/text.rst',
        # the classic function "open" can be used, but if the value is
        # '__pyclasspath__/docutils/writers/html4css1/html4css1.css',
        # python doesn't be able to find the resource, so it must have the
        # absolute path to the jar to read it with jaropen after.
        # ex: jar:file:/home/user/workspace/jrst/docutils/docutils.jar!
        def jaropen(resourcePath, mode, bufsize=-1):
            if "__pyclasspath__" in resourcePath:
                # Replace the relative resource filepath by the absolute path
                docutilsAbsolutePath =  "jar:file:" + os.path.dirname(docutilsPath) + "!"
                resourcePath = string.replace(resourcePath, "__pyclasspath__", docutilsAbsolutePath)
                # Get the url and open a stream to access to the resource
                # /!\ Warning : we use a deprecated function, it could be a problem later
                url = URL(resourcePath)
                inStream = url.openStream()
                f = __builtin__.openlegacy(inStream, bufsize)
            else:
                f = __builtin__.openlegacy(resourcePath, mode, bufsize)
            return f
        
        # Replace in the dictionnary the old open function by the new one (openlegacy)
        __builtin__.__dict__["openlegacy"]  = __builtin__.__dict__["open"]
        __builtin__.__dict__["open"] = jaropen

def exec_docutils (docutilsPath, typeOutput, fileIn):

    # Initalization before using Docutils
    init_docutils(docutilsPath)
    
    # Add the lib path to sys.path
    lib_path = os.path.dirname(docutilsPath)
    sys.path.append(lib_path)

    # Add the scripts path to sys.path
    scriptpath = lib_path + '/build/scripts-2.7'
    sys.path.append(scriptpath)

    typeOutput = typeOutput.lower()

    # Check if the output type is supported by Docutils
    listType = ["xml", "html", "odt", "latex", "man", "s5", "xetex"]
    if typeOutput in listType:
        from docutils.core import publish_file
        publish_file( source_path=fileIn, writer_name=typeOutput )
    else:
        print "Wrong output format"

