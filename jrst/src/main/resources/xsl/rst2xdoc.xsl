<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  JRst :: Api
  %%
  Copyright (C) 2004 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
	xmlns="http://www.w3.org/TR/xhtml1/strict">

  <!-- xdoc is mostly an xhtml document wrapped inside a DOCUMENT tag -->
  <xsl:import href="rst2xhtml.xsl"/>

  <xsl:template match="/document">
    <document>
        <properties>
            <title><xsl:value-of select="title"/></title>
        </properties>
        <body>
            <section>
              <xsl:attribute name="name"><xsl:value-of select="title"/></xsl:attribute>
              <xsl:apply-templates />
            </section>
        </body>
    </document>
  </xsl:template>

  <xsl:template match="section">
	<a name="{@ids}" id="{@ids}"></a>
      <xsl:choose>
        <xsl:when test="count(ancestor::section) = 0">
          <xsl:element name="subsection">
            <xsl:if test="@names">
              <xsl:attribute name="name"><xsl:value-of select="title"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="@name">
              <xsl:attribute name="name"><xsl:value-of select="title"/></xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
          </xsl:element>
        </xsl:when>
        <xsl:when test="count(ancestor::section) &gt;= 1">
          <!--There are only 2 levels of section/subsection, so after we use
          <h4>, <h5>, etc... for titles-->
          <xsl:element name="h{count(ancestor::section) + 3}">
            <xsl:value-of select="title"/>
          </xsl:element>
          <xsl:apply-templates/>
        </xsl:when>
      </xsl:choose>
  </xsl:template>

  <xsl:template match="title">
    <xsl:choose>
      <xsl:when test="count(ancestor::section) &gt;= 1">
        <!-- If there is a subsection, don't display the title twice
        ( so we must do nothing) -->
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- #286 : Régression sur la génération des méta-données
  TODO peut etre que les xsl de xhtml et xdoc devrait être indépendante -->
  <xsl:template match="docinfo">
    <table class="bodyTable" border="0">
      <col class="docinfo-name" />
      <col class="docinfo-content" />
      <tbody valign="top">
        <xsl:apply-templates/>
      </tbody>
    </table>
  </xsl:template>

</xsl:transform>

