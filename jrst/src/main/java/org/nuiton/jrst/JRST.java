/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.jrst.convertisor.DocUtils2RST;
import org.nuiton.jrst.convertisor.DocUtilsVisitor;
import org.nuiton.jrst.legacy.JRSTReader;
import org.nuiton.jrst.ui.JRSTView;
import org.nuiton.util.Resource;
import org.nuiton.util.StringUtil;
import org.python.util.PythonInterpreter;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 * FIXME: 'JRST --help' doesn't work, but 'JRST --help toto' work :( FIXME:
 * 'JRST -c' doesn't work, but 'JRST -c toto'
 * 
 * Created: 3 nov. 06 20:56:00
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class JRST {

    public static final String UTF_8 = "UTF-8";

    public static final String DOCUTILS_LAUNCHER = "__run__.py";

    public static final String IMPORT_SCRIPT = "import __run__";

    public static final String WINDOWS_NAME = "win";

    public static final String OS_NAME = "os.name";

    public static final String BANG = "!";

    public static final String FILE_URI_PREFIX = "file:";

    public enum Overwrite {
        NEVER, IFNEWER, ALLTIME
    }

    /** to use log facility, just put in your code: log.info("..."); */
    protected static Log log = LogFactory.getLog(JRST.class);

    /** XSL Stylesheet to transform RST into HTML. */
    protected static final String RST_2_XHTML = "/xsl/rst2xhtml.xsl";

    /** XSL Stylesheet to transform RST into HTML (but only inner body fragment). */
    protected static final String XSL_RST_2_XHTML_INNER_BODY_XSL = "/xsl/rst2xhtmlInnerBody.xsl";

    /** XSL Stylesheet to transform RST into Xdoc. */
    protected static final String RST_2_XDOC = "/xsl/rst2xdoc.xsl";

    /** XSL Stylesheet to transform RST into Docbook. */
    protected static final String RST_2_DOCBOOK = "/xsl/dn2dbk.xsl";

    /** XSL Stylesheet to transform Docbook into xHTML. */
    protected static final String DOCBOOK_2_XHTML = "/docbook/xhtml/docbook.xsl";

    /** XSL Stylesheet to transform Docbook into javahelp. */
    protected static final String DOCBOOK_2_JAVAHELP = "/docbook/javahelp/javahelp.xsl";

    /** XSL Stylesheet to transform Docbook into htmlhelp. */
    protected static final String DOCBOOK_2_HTMLHELP = "/docbook/htmlhelp/htmlhelp.xsl";

    /** XSL Stylesheet to transform Docbook into PDF. */
    protected static final String DOCBOOK_2_FO = "/docbook/fo/docbook.xsl";

    public static final String PATTERN_TYPE = "xml|xhtml|docbook|html|htmlInnerBody|xdoc|fo|pdf";

    /** HTML output format type */
    public static final String TYPE_HTML = "html";

    /** HTML output format type */
    public static final String TYPE_HTML_INNER_BODY = "htmlInnerBody";

    /** XDOC output format type */
    public static final String TYPE_XDOC = "xdoc";

    /** DOCBOOK output format type */
    public static final String TYPE_DOCBOOK = "docbook";

    /** XHTML output format type */
    public static final String TYPE_XHTML = "xhtml";

    /** JAVA HELP output format type */
    public static final String TYPE_JAVAHELP = "javahelp";

    /** HTML HELP output format type */
    public static final String TYPE_HTMLHELP = "htmlhelp";

    /** ODT output format type */
    public static final String TYPE_ODT = "odt";

    /** FO output format type */
    public static final String TYPE_FO = "fo";

    /** PDF output format type */
    public static final String TYPE_PDF = "pdf";

    /** XML output format type */
    public static final String TYPE_XML = "xml";

    /** key, Out type; value: chain of XSL file to provide wanted file for output */
    protected static Map<String, String> stylesheets;

    /** Mime type associated with type */
    protected static Map<String, String> mimeType;

    static {
        stylesheets = new HashMap<String, String>();
        stylesheets.put(TYPE_HTML, RST_2_XHTML);
        stylesheets.put(TYPE_HTML_INNER_BODY, XSL_RST_2_XHTML_INNER_BODY_XSL);
        stylesheets.put(TYPE_XDOC, RST_2_XDOC);
        stylesheets.put(TYPE_DOCBOOK, RST_2_DOCBOOK);
        stylesheets.put(TYPE_XHTML, RST_2_DOCBOOK + "," + DOCBOOK_2_XHTML);
        stylesheets.put(TYPE_JAVAHELP, RST_2_DOCBOOK + "," + DOCBOOK_2_JAVAHELP);
        stylesheets.put(TYPE_HTMLHELP, RST_2_DOCBOOK + "," + DOCBOOK_2_HTMLHELP);
        stylesheets.put(TYPE_FO, RST_2_DOCBOOK + "," + DOCBOOK_2_FO);
        stylesheets.put(TYPE_PDF, RST_2_XHTML);

        mimeType = new HashMap<String, String>();
        mimeType.put(TYPE_HTML, "text/html");
        mimeType.put(TYPE_HTML_INNER_BODY, "text/html");
        mimeType.put(TYPE_XDOC, "text/xml");
        mimeType.put(TYPE_DOCBOOK, "text/xml");
        mimeType.put(TYPE_XHTML, "text/html");
        mimeType.put(TYPE_JAVAHELP, "text/plain");
        mimeType.put(TYPE_HTMLHELP, "text/html");
        mimeType.put(TYPE_ODT, "application/vnd.oasis.opendocument.text");
        mimeType.put(TYPE_FO, "text/xml");
        mimeType.put(TYPE_PDF, "application/pdf");

    }

    /**
     * Main method.
     *
     * @param args main args
     * @throws Exception
     */
    public static void main(String... args) throws Exception {

        I18n.init(new ClassPathI18nInitializer(), Locale.UK);

        if (args.length == 0) {
            askOption();
        } else {

            ApplicationConfig config = JRSTConfig.getConfig(args);
            config.doAction(0);

            // parse options
            String xslList = JRSTConfigOption.XSL_FILE.getOption(config);
            if (xslList == null) {
                xslList = JRSTConfigOption.OUT_TYPE.getOption(config);
            }
            List<String> unparsed = config.getUnparsed();
            if (unparsed.isEmpty()) {
                JRSTConfig.help();
            }
            File inputFile = new File(config.getUnparsed().get(0));
            File ouputFile = JRSTConfigOption.OUT_FILE.getOptionAsFile(config);
            Overwrite overwrite = Overwrite.NEVER;
            if (JRSTConfigOption.FORCE.getOptionAsBoolean(config)) {
                overwrite = Overwrite.ALLTIME;
            }
            boolean simpleGeneration = false;
            if (JRSTConfigOption.SIMPLE.getOptionAsBoolean(config)) {
                simpleGeneration = true;
            }

            generate(xslList, inputFile, ouputFile, overwrite, simpleGeneration);
        }
    }

    private static void askOption() throws SecurityException,
            NoSuchMethodException, IOException {
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            if (!(gs == null)) {
                if (gs.length > 0) {
                    askOptionGraph();
                }
            }
        } catch (java.awt.HeadlessException e) {
            log.error("Can't generate document", e);
        }
    }

    /**
     * Graphical user interface
     *
     * @throws SecurityException
     * @throws NoSuchMethodException
     */
    protected static void askOptionGraph() throws SecurityException,
            NoSuchMethodException {
        JRSTView jrstView = new JRSTView();
        jrstView.pack();
        jrstView.setVisible(true);
    }

    /**
     * Transforms a Restructured Text (ReST) file to another type ( html, xdoc, pdf, etc... )
     *
     * @param outputType The type of the output file ( html, xdoc, pdf, etc... )
     * @param fileIn     The restructured text input file (rst)
     * @param fileOut    The output file
     * @param overwrite  The rule to overwrite file (NEVER, IFNEWER or ALLTIME )
     * @throws Exception
     */
    public static void generate(String outputType, File fileIn,
                                File fileOut, Overwrite overwrite,
                                boolean simpleGeneration) throws Exception {
        if (fileOut != null
            && fileOut.exists()
            && (overwrite == Overwrite.NEVER || (overwrite == Overwrite.IFNEWER && FileUtils
                .isFileNewer(fileIn, fileOut)))) {

            log.info("Don't generate file " + fileOut
                     + ", because already exists");
        } else {
            Document doc;

            JRSTToXmlStrategy strategy;
            if (simpleGeneration) {
                strategy = new JRSTToXmlStrategyJRSTReader();
            } else {
                strategy = new JRSTToXmlStrategyDocutils();
            }

            doc = strategy.generateRstToXml(fileIn, UTF_8);

            // Application of xsl stylesheets
            doc = generateXml(doc, outputType);

            // generation PDF
            if (outputType.equals("pdf")) {
                generatePdf(doc, fileIn, fileOut);
            } else {
                generateFile(doc, fileOut);
            }
        }
    }

    /**
     * Transforms a Restructured Text (ReST) file to pdf
     *
     * @param fileIn    The restructured text input file (rst)
     * @param fileOut   The output file
     * @param overwrite The rule to overwrite file (NEVER, IFNEWER or ALLTIME )
     * @param doc       document to generate
     * @throws Exception
     */
    public static void generatePdf(File fileIn, File fileOut, Overwrite overwrite,
                                   Document doc) throws Exception {
        if (fileOut != null
            && fileOut.exists()
            && (overwrite == Overwrite.NEVER || (overwrite == Overwrite.IFNEWER && FileUtils
                .isFileNewer(fileIn, fileOut)))) {

            log.info("Don't generate file " + fileOut
                     + ", because already exists");
        } else {

            // Application of xsl stylesheets
            doc = generateXml(doc, "pdf");

            // generation PDF
            generatePdf(doc, fileIn, fileOut);

        }
    }

    /**
     * Transforms a restructured text file to a XML file using JRST parser (used with option --simple)
     *
     * @param fileIn   Input restructured text file (.rst)
     * @param encoding Output file encoding
     * @return A document which contains XML code
     * @throws Exception
     */
    public static Document generateSimpleDoc(File fileIn, String encoding) throws Exception {
        URL url = fileIn.toURI().toURL();
        Reader in = new InputStreamReader(url.openStream(), encoding);

        // parse rst file
        JRSTReader jrst = new JRSTReader();
        return jrst.read(in);
    }

    /**
     * Transforms a restructured text file to a XML file using Jython interpreter to execute DocUtils scripts.
     *
     * @param in       Input restructured text file (.rst)
     * @param encoding Output file encoding
     * @return A document which contains XML code
     * @throws Exception
     */
    public static Document generateDocutils(File in, String encoding) throws Exception {

        ByteArrayOutputStream out = null;

        try {
            // Transformation to XML
            out = new ByteArrayOutputStream();

            // Transformation of the __run__ URL into a path that python will use
            // For example the URL is :
            // jar:file:/home/user/.m2/repository/org/nuiton/jrst/docutils/1.6-SNAPSHOT/docutils-1.6-SNAPSHOT.jar!/__run__.py
            // and it becomes :
            // /home/user/.m2/repository/org/nuiton/jrst/docutils/1.6-SNAPSHOT/docutils-1.6-SNAPSHOT.jar/
            URL resource = JRST.class.getResource("/" + DOCUTILS_LAUNCHER);
            String docutilsPath = resource.getPath()
                    .replaceAll(DOCUTILS_LAUNCHER, "");

            docutilsPath = docutilsPath.replaceAll(BANG, "");
            docutilsPath = docutilsPath.replaceAll(FILE_URI_PREFIX, "");

            // Import of the main script to use docutils ( __run__ )
            PythonInterpreter interp = new PythonInterpreter();
            String commandImport = IMPORT_SCRIPT;
            interp.exec(commandImport);

            // If the OS is windows, escapes the backslashs in the filepath
            String filePath = in.getAbsolutePath();
            String property = System.getProperty(OS_NAME).toLowerCase();
            if (property.contains(WINDOWS_NAME)) {
                filePath = filePath.replaceAll("\\\\", "\\\\\\\\");
            }

            // Sets an output stream in the python interpreter and executes the code
            interp.setOut(out);

            // Execution of the docutils script to transform rst to xml
            String commandExec = String.format("__run__.exec_docutils('%s', '%s', '%s')",
                                               docutilsPath, TYPE_XML, filePath);
            interp.exec(commandExec);

            // Cleans the python interpreter to avoid problems if they are multiple execution of this method
            interp.cleanup();

            // Transforms the output stream to a document
            String xmlString = new String(out.toByteArray(), encoding);

            Document doc = null;
            try {
                doc = DocumentHelper.parseText(xmlString);
            } catch (DocumentException e) {
                log.error("Error during the creation of the document", e);
            }

            return doc;
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * Applies XSL stylesheet(s) to a XML document
     *
     * @param doc              A document which contains XML code
     * @param xslListOrOutType String which describes transformations to apply to the XML document
     * @return A document which contains XML transformed by XSL stylesheets
     * @throws IOException
     * @throws TransformerException
     */
    public static Document generateXml(Document doc, String xslListOrOutType) throws IOException, TransformerException {

        // search xsl file list to apply
        String xslList = stylesheets.get(xslListOrOutType);
        if (xslListOrOutType == null) {
            xslList = xslListOrOutType;
        }

        // apply xsl on rst xml document
        JRSTGenerator gen = new JRSTGenerator();
        String[] xsls = StringUtil.split(xslList, ",");
        for (String xsl : xsls) {
            URL stylesheet;
            File file = new File(xsl);
            if (file.exists()) {
                stylesheet = file.toURI().toURL();
            } else {
                stylesheet = Resource.getURL(xsl);
            }
            if (stylesheet == null) {
                throw new FileNotFoundException("Can't find stylesheet: "
                                                + xsl);
            }

            // add entity resolver
            gen.setUriResolver(new JRSTResourceResolver(xsl));

            // do transformation
            doc = gen.transform(doc, stylesheet);
        }
        return doc;
    }

    /**
     * Writes the XML content generated in a file.
     *
     * @param doc     Document which contains XML code
     * @param fileOut Output file
     * @throws IOException
     */
    public static void generateFile(Document doc, File fileOut) throws IOException {

        // Out
        OutputStreamWriter writer = null;

        try {
            OutputStream outputStream = new FileOutputStream(fileOut);

            // If the output file type si not "pdf", we can write in the final file
            writer = new OutputStreamWriter(outputStream, UTF_8);

            // write generated document
            writer.write(doc.asXML());
        } catch (Exception eee) {
            log.error("Failed to write file", eee);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * Generates PDF file with IText with an HTML document.
     *
     * @param result  Document which contains HTML code
     * @param fileIn  ReST file used to build resources path
     * @param fileOut PDF Output file
     * @throws Exception
     */
    public static void generatePdf(Document result, File fileIn, File fileOut) throws Exception {

        // Out
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(fileOut);

            // Creation of the document builder
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            builder.setEntityResolver(result.getEntityResolver());

            // We must redifine our own DocumentInputSource because of the visibility
            ByteArrayInputStream inputStream = new ByteArrayInputStream(result.asXML().getBytes());
            org.w3c.dom.Document doc = builder.parse(inputStream);

            ITextRenderer renderer = new ITextRenderer();

            // Settings to resolve paths with the JRST User Agent
            String absolutePath = fileIn.getParentFile().getAbsolutePath();
            JRSTUserAgent jrstUserAgent = new JRSTUserAgent(absolutePath);
            jrstUserAgent.setBaseURL(absolutePath);
            renderer.getSharedContext().setUserAgentCallback(jrstUserAgent);
            jrstUserAgent.setSharedContext(renderer.getSharedContext());

            // Generation of the pdf file
            renderer.setDocument(doc, null);
            renderer.layout();
            renderer.createPDF(outputStream);
        } catch (Exception eee) {
            log.error("Failed to write PDF", eee);
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    /**
     * Method used to generate rst document.
     * 
     * <b>WARN : don't work !</b>
     *
     * @param doc docutils document to generate
     * @return rst document
     * @throws IOException
     */
    public static String generateRST(Document doc) throws IOException {
        // Creation d'un visitor qui convertie de l'xml vers le rst
        DocUtilsVisitor visitor = new DocUtils2RST();

        // Attacher le visitor au document
        // il va parcourir tout les elements et reconstruire du rst
        doc.accept(visitor);

        // Recuperation du resultat
        String result = visitor.getResult();
        // nettoyage du visiteur
        visitor.clear();

        return result;
    }
}
