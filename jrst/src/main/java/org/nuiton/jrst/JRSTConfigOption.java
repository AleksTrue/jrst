/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

import java.io.File;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

/**
 * JRST options enum.
 * 
 * @author chatellier
 */
public enum JRSTConfigOption implements ConfigOptionDef {

    FORCE("force", n("jrst.option.force"), "false", String.class, true, false),
    SIMPLE("simple", n("jrst.option.simple"), "false", String.class, true, false),
    OUT_FILE("outFile", n("jrst.option.outfile"), null, String.class, true, false),
    OUT_TYPE("outType", n("jrst.option.outtype"), null, String.class, true, false),
    XSL_FILE("xslFile", n("jrst.option.xslfile"), null, String.class, true, false),
    INTERMEDIATE_FILE("intermediateFile", n("jrst.option.intermediatefile"), JRST.TYPE_XHTML,
            String.class, true, false);

    public String key;
    public String description;
    public String defaultValue;
    public Class<?> type;
    public boolean isTransient;
    public boolean isFinal;
    public String[] alias;

    private JRSTConfigOption(String key, String description,
            String defaultValue, Class<?> type, boolean isTransient, boolean isFinal, String... alias) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isFinal;
        this.alias = alias;
    }

    @Override
    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public boolean isTransient() {
        return isTransient;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String getDescription() {
        return t(description);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean isTransient) {
        this.isTransient = isTransient;
    }

    @Override
    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public String[] getAlias() {
        return alias;
    }
    
    public String getOption(ApplicationConfig config) {
        String result = config.getOption(getKey());
        return result;
    }
    
    public File getOptionAsFile(ApplicationConfig config) {
        File result = config.getOptionAsFile(getKey());
        return result;
    }
    
    public boolean getOptionAsBoolean(ApplicationConfig config) {
        boolean result = config.getOptionAsBoolean(getKey());
        return result;
    }
}
