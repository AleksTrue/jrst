/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import org.codehaus.plexus.component.annotations.Component;
import org.dom4j.Document;
import org.nuiton.jrst.legacy.JRSTReader;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * Old mecanism to transform rst file to xml format using {@link JRSTReader}.
 *
 * @author tchemit (chemit@codelutin.com)
 * @since 2.0.1
 */
@Component(role = JRSTToXmlStrategy.class, hint = "legacy",
           description = "Transform a RST model (using jrst java api), " +
                         "to a xml format.")
public class JRSTToXmlStrategyJRSTReader implements JRSTToXmlStrategy {

    @Override
    public Document generateRstToXml(File fileIn, String encoding) throws Exception {
        URL url = fileIn.toURI().toURL();
        Reader in = new InputStreamReader(url.openStream(), encoding);

        // parse rst file
        JRSTReader jrst = new JRSTReader();
        return jrst.read(in);
    }
}
