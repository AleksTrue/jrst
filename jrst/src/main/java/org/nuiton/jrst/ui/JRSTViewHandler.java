/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.ui;

import com.google.common.io.Files;
import java.awt.Color;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import jaxx.runtime.context.JAXXInitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jrst.JRST;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 03/05/12
 *
 * @author jpages
 */
public class JRSTViewHandler {
    /** to use log facility, just put in your code: log.info("..."); */
    protected static Log log = LogFactory.getLog(JRSTViewHandler.class);

    protected JRSTView jrstView;

    public JRSTViewHandler(JRSTView jrstView) {
        this.jrstView = jrstView;
    }

    public void init() {
        ButtonGroup group = new ButtonGroup();
        group.add(jrstView.getXslRadio());
        group.add(jrstView.getFormat());

        addXslLocation();
    }

    public int askEcraser() {
        return JOptionPane.showConfirmDialog(jrstView, t("overwriteGraph?"));
    }

    public String[] getFormats() {
        String[] formats = JRST.PATTERN_TYPE.split("\\|");
        return formats;
    }

    public void cancel() {
        System.exit(0);
    }

    public void addXslLocation() {
        JAXXInitialContext context = new JAXXInitialContext();
        context.add(jrstView);
        JRSTCommandModel model = jrstView.getModel();
        context.add(model);
        XslPanel xslPanel = new XslPanel(context);
        xslPanel.setModel(model);
        xslPanel.setModel(model);
        int panelNumber = model.getPanelNumber();
        xslPanel.setPanelNumber(panelNumber);
        JPanel xslListPanel = jrstView.getXslListPanel();
        xslListPanel.add(xslPanel);
        jrstView.pack();
    }

    public void convert() {
        if (jrstView.getOpenLocation() == null) {
            jrstView.getErrorLbl().setText(t("openEmpty?"));
            jrstView.getErrorLbl().setForeground(Color.RED);
            jrstView.pack();
        } else {
            launchJRST();
            jrstView.setVisible(false);
        }
    }

    public void launchJRST() {
        JRSTCommandModel model = jrstView.getModel();
        File fileIn = model.getOpenFile();

        if (fileIn != null) {
            String xslListOrFormat;
            if (model.isFormatEnabled())
                xslListOrFormat = model.getSelectedFormat();
            else {
                xslListOrFormat = model.getXsls();
            }
            File outputFile = model.getSaveFile();
            if (outputFile.exists()) {
                int choix = jrstView.getHandler().askEcraser();
                if (choix == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            if (model.getSaveFile() == null) {
                // If the user didn't give the output file, we take the input file name with the right extension
                // For example, nameExample.rst becomes nameExample.xml
                if (outputFile.isFile()) {
                    String outputPath = fileIn.getAbsolutePath();
                    String ext = Files.getFileExtension(outputPath);
                    if (ext.isEmpty()) {
                        outputPath = outputPath + "." + xslListOrFormat;
                    } else {
                        outputPath = outputPath.replace("." + ext, "." + xslListOrFormat);
                    }
                    outputFile = new File(outputPath);
                }
            }

            try{
                JRST.generate(xslListOrFormat, fileIn, outputFile, JRST.Overwrite.ALLTIME, model.isSimpleMode());
            } catch (Exception e) {
                log.error("Can't generate the document with this configuration", e);
            } finally {
                jrstView.dispose();
            }
        }
    }
}
