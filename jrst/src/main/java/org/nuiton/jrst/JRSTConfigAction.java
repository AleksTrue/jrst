/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

import org.nuiton.config.ConfigActionDef;

/**
 * JRST actions.
 * 
 * @author echatellier
 */
public enum JRSTConfigAction implements ConfigActionDef {
    HELP(n("JRST help"), JRSTConfig.class.getName() + "#help", "-h", "--help");

    public String description;
    public String action;
    public String[] aliases;

    private JRSTConfigAction(String description, String action, String... aliases) {
        this.description = description;
        this.action = action;
        this.aliases = aliases;
    }

    public String getDescription() {
        return t(description);
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String[] getAliases() {
        return aliases;
    }

}
