/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FasterCachedResourceResolver;
import org.nuiton.util.Resource;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * @author sletellier
 * @since 1.5
 */
public class JRSTResourceResolver extends FasterCachedResourceResolver {

    static private Log log = LogFactory.getLog(JRSTResourceResolver.class);

    protected String xsl;

    public JRSTResourceResolver(String xsl) {
        this.xsl = xsl;
    }

    @Override
    public Source resolve(String href, String base) {

        // Try first with faster resource resolver
        Source resolved = super.resolve(href, base);
        if (resolved != null) {
            return resolved;
        }

        // If not found, try to find in xsl
        if (log.isDebugEnabled()) {
            log.debug("RESOLVING: href: "+href+" base: "+base);
        }
        String xslDir = xsl.substring(0, xsl.lastIndexOf('/') + 1);
        String resouceFullName = xslDir + href;

        if (log.isDebugEnabled()) {
            log.debug("RESOLVING: resouceFullName: "+resouceFullName);
        }

        try {
            InputStream stream;
            File file = new File(resouceFullName);
            if (file.exists()) {
                stream = new FileInputStream(file);
            } else {
                try {
                    stream = Resource.getURL(resouceFullName).openStream();
                }
                catch (Exception e) {
                    stream = Resource.getURL("/docbook/common/" + href).openStream();
                }
            }

            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = parserFactory.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId)
                        throws SAXException, IOException {

                    InputSource source = null;

                    String smallSystemId = systemId.substring(systemId.lastIndexOf("/") + 1);
                    URL url = Resource.getURL("/docbook/common/" + smallSystemId);
                    if (url != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Resolved entity url : " + url);
                        }
                        InputStream stream = url.openStream();
                        source = new InputSource(stream);
                    }

                    return source;
                }
            });
            InputSource inSrc = new InputSource(stream);
            SAXSource saxs = new SAXSource(xmlReader, inSrc);
            return saxs;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        }
    }
}
