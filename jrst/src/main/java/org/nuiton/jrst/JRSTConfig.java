/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

/**
 * Permet la configuration de JRST pour son execution (parsing de la ligne
 * de commande).
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class JRSTConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(JRSTConfig.class);

    private JRSTConfig() {
    }


    static public ApplicationConfig getConfig() {
        return getConfig(null, null);
    }

    static public ApplicationConfig getConfig(String[] args) {
        return getConfig(null, null, args);
    }

    /**
     * Create WikittyConfig and load particular configuration filename.
     * 
     * @param configFilename name of wikitty config file
     */
    static public ApplicationConfig getConfig(String configFilename) {
        return getConfig(null, configFilename);
    }

    /**
     * Create WikittyConfig and use props as default value
     *
     * @param props as default value
     */
    static public ApplicationConfig getConfig(Properties props) {
        return getConfig(props, null);
    }

    static public ApplicationConfig getConfig(
            Properties props, String configFilename, String ... args) {
        ApplicationConfig conf = new ApplicationConfig(props, configFilename);
        conf.loadDefaultOptions(JRSTConfigOption.values());
        conf.loadActions(JRSTConfigAction.values());
        
        // add options alias
        conf.addAlias("--console", "--option", "console", "true");
        conf.addAlias("-c", "--option console true");
        conf.addAlias("--force", "--option", "force", "true");
        conf.addAlias("--outFile", "--option", "outFile");
        conf.addAlias("-o", "--option", "outFile");
        conf.addAlias("--outType", "--option", "outType");
        conf.addAlias("-t", "--option", "outType");
        conf.addAlias("--xslFile", "--option", "xslFile");
        conf.addAlias("-x", "--option", "xslFile");
        conf.addAlias("-i", "--intermediate", "intermediateFile");
        conf.addAlias("--simple", "--option", "simple", "true");
        conf.addAlias("-s", "--option", "simple", "true");

        try {
            conf.parse(args);
        } catch (ArgumentsParserException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load wikitty configuration", eee);
            }
        }
        return conf;
    }    

    static public void help() {
        System.out.println("JRST configuration and action");
        System.out.println("Options (set with --option <key> <value>:");
        for (JRSTConfigOption o : JRSTConfigOption.values()) {
            System.out.println("\t" + o.key + "(" + o.getDefaultValue() + ")\t:" + o.getDescription());
        }

        System.out.println("Actions:");
        for (JRSTConfigAction a : JRSTConfigAction.values()) {
            System.out.println("\t" + java.util.Arrays.toString(a.aliases) + "(" + a.getAction() + ")\t:" + a.getDescription());
        }
        System.exit(0);
    }

}
