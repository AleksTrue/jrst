/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.Resource;

/**
 *
 * @author chemit
 */
public class JRSTTest extends JRSTAbstractTest {

    /** to use log facility, just put in your code: log.info("..."); */
    protected static Log log = LogFactory.getLog(JRSTTest.class);

    /**
     * Test que les ressources sont bien dans le classpath.
     * 
     * Test du cas particulier de la dependance docbook-xsl.
     */
    @Test
    public void testResources() {
        Assert.assertNotNull(Resource.getURL(JRST.RST_2_XHTML));
        Assert.assertNotNull(Resource.getURL(JRST.RST_2_XDOC));
        Assert.assertNotNull(Resource.getURL(JRST.RST_2_DOCBOOK));
        Assert.assertNotNull(Resource.getURL(JRST.DOCBOOK_2_XHTML));
        Assert.assertNotNull(Resource.getURL(JRST.DOCBOOK_2_JAVAHELP));
        Assert.assertNotNull(Resource.getURL(JRST.DOCBOOK_2_HTMLHELP));
        Assert.assertNotNull(Resource.getURL(JRST.DOCBOOK_2_FO));
    }

    @Test
    public void generateXml() throws Exception {

        File in = getTestFile("test.rst");
        File out = getOutputTestFile("jrst-RstToXml.xml");

        generate(JRST.TYPE_XML, in, out, JRST.Overwrite.ALLTIME);
    }

    @Test
    public void generateSimpleXml() throws Exception {

        File in = getTestFile("test.rst");
        File out = getOutputTestFile("jrst-RstToSimpleXml.xml");

        generate(JRST.TYPE_XML, in, out, JRST.Overwrite.ALLTIME);
    }
}
