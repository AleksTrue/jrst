/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.nuiton.util.TimeLog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author sletellier
 */
public class JRSTAbstractTest extends Assert {

    /** to use log facility, just put in your code: log.info("..."); */
    protected static Log log = LogFactory.getLog(JRSTAbstractTest.class);

    protected static File testWorkDir;
    protected static File testResourcesDir;

    @BeforeClass
    public static void initTest() throws IOException {
        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        File basedirFile = new File(basedir);
        testWorkDir = new File(basedirFile,
                               "target" + File.separator +
                               "surefire-workdir");

        boolean b = testWorkDir.exists() || testWorkDir.mkdirs();
        if (!b) {
            throw new IOException(
                    "Could not create workdir directory " + testWorkDir);
        }

        testResourcesDir = new File(basedirFile,
                                    "src" + File.separator +
                                    "test" + File.separator +
                                    "resources" + File.separator);
    }

    public static File getOutputTestFile(String fileName) throws IOException {
        File file = new File(testWorkDir,System.nanoTime()+"_"+ fileName);
        file.createNewFile();
        //file.deleteOnExit();
        return file;
    }

    public static String getOutputTestPath(String fileName) throws IOException {
        File file = getOutputTestFile(fileName);
        return file.getPath();
    }

    public static File getTestFile(String testName) {
        return new File(testResourcesDir, testName);
    }

    public static File getBugTestFile(String testName) {
        return new File(testResourcesDir, File.separator + "bugs" + File.separator + testName);
    }

    public static InputStream getTestStream(String testName) {
        return JRSTAbstractTest.class.getResourceAsStream("/" + testName);
    }

    public static class JRSTTestGenerator {
        protected String outputType;
        protected File in;
        protected File out;
        protected JRST.Overwrite overwrite;

        public JRSTTestGenerator(String outputType, File in, File out, JRST.Overwrite overwrite) throws Exception {
            this.outputType = outputType;
            this.in = in;
            this.out = out;
            this.overwrite = overwrite;
            generate();
        }

        public void generate() throws Exception {
            generateSimple();
            generateDocutils();
        }

        protected void generateSimple() throws Exception {
            TimeLog simpleModeTimeLog = new TimeLog("Simple Mode");
            simpleModeTimeLog.setTimeToLogInfo(100l);
            Long start = TimeLog.getTime();
            JRST.generate(outputType, in, out, overwrite, true);
            simpleModeTimeLog.log(start, "Simple Mode generation");
            assertJRST(in, out);
        }

        protected void generateDocutils() throws Exception {
            TimeLog docutilsTimeLog = new TimeLog("DocUtils");
            Long start = TimeLog.getTime();
            JRST.generate(outputType, in, out, overwrite, false);
            docutilsTimeLog.log(start, "DocUtils generation");
            assertJRST(in, out);
        }

        /**
         * Overide this method to add assert
         * @param in
         * @param out
         * @throws Exception
         */
        public void assertJRST(File in, File out) throws Exception {
        }
    }

    public void generate(String outputType, File in, File out, JRST.Overwrite overwrite) throws Exception {
        new JRSTTestGenerator(outputType, in, out, overwrite);
    }
}
