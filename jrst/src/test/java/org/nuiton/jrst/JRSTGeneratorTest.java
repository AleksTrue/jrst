/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

/**
 * JRSTGeneratorTest.
 *
 * Created: 31 oct. 06 11:14:19
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class JRSTGeneratorTest extends JRSTAbstractTest {

    /** to use log facility, just put in your code: log.info("..."); */
    protected static Log log = LogFactory.getLog(JRSTGeneratorTest.class);

    public static File getResourcesTestPath() {
        return getTestFile("test.rst");
    }

    // Not implemented
    @Ignore
    @Test
    public void testRstToRst() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to RST (test.rst)...");
        }

        String test1 = getOutputTestPath("jrst-RstToRst.rst");
        new JRSTTestGenerator("rst", getResourcesTestPath(), new File(test1), JRST.Overwrite.ALLTIME);
    }

    @Test
    public void testRstToHtml() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to HTML (test.rst)...");
        }

        String test1 = getOutputTestPath("jrst-RstToHtml.html");
        new JRSTTestGenerator("html", getResourcesTestPath(), new File(test1), JRST.Overwrite.ALLTIME);
    }

    @Test
    public void testRstToHtml2() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Testing RST to HTML (docDeveloppeur.rst) ...");
        }

        String test1 = getOutputTestPath("jrst-RstToHtml2.html");
        new JRSTTestGenerator("html", getTestFile("docDeveloppeur.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }


    @Test
    public void testRstToDocbook() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Docbook (test.rst)...");
        }

        String test1 = getOutputTestPath("jrst-RstToDocbook.dbk");
        new JRSTTestGenerator("docbook", getResourcesTestPath(), new File(test1), JRST.Overwrite.ALLTIME);
    }
    
    @Test
    public void testRstToXdoc() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Xdoc (test.rst)...");
        }
        
        String test1 = getOutputTestPath("jrst-RstToXdoc.xdoc");
        new JRSTTestGenerator("xdoc", getResourcesTestPath(), new File(test1), JRST.Overwrite.ALLTIME);
    }

     // This test is not working.
    @Ignore
    public void testRstToXdoc2() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Xdoc (test2.rst)...");
        }
        
        String test1 = getOutputTestPath("jrst-RstToXdoc2.xdoc");
        new JRSTTestGenerator("xdoc", getTestFile("test2.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }
    
    @Test
    public void testRstToXdoc3() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Xdoc (test3.rst) ...");
        }
        
        String test1 = getOutputTestPath("jrst-RstToXdoc3.xdoc");
        new JRSTTestGenerator("xdoc", getTestFile("test3.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }
    
    @Test
    public void testRstToXdoc4() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Xdoc (test4.rst) ...");
        }
        
        String test1 = getOutputTestPath("jrst-RstToXdoc4.xdoc");
        new JRSTTestGenerator("xdoc", getTestFile("test4.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }
    
    @Test
    public void testRstToXdocJrstSite() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to Xdoc (frEntier.rst) ...");
        }
        
        String test1 = getOutputTestPath("jrst-RstToXdocJrstSite.xdoc");
        new JRSTTestGenerator("xdoc", getTestFile("frEntier.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }


    @Test
    public void testRstToPDF() throws Exception {
        
        if (log.isInfoEnabled()) {
            log.info("Testing RST to PDF (test.rst) ...");
        }

        String test1 = getOutputTestPath("jrst-RstToPDF.pdf");
        new JRSTTestGenerator("pdf", getResourcesTestPath(), new File(test1), JRST.Overwrite.ALLTIME);
    }

    @Test
    public void testRstToPDF2() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Testing RST to PDF (docDeveloppeur.rst) ...");
        }

        String test1 = getOutputTestPath("jrst-RstToPDF2.pdf");
        new JRSTTestGenerator("pdf", getTestFile("docDeveloppeur.rst"), new File(test1), JRST.Overwrite.ALLTIME);
    }
}
