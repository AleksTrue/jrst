/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;

/**
 * Test concernant les titres en général.
 *
 * @author chatellier
 */
public class TitlesTest extends JRSTAbstractTest {

    /**
     * Test que la génération fonctionne meme s'il n'y a pas de titre de niveau 2.
     */
    @Test
    public void testNoSubtitle() throws Exception {
        File in = new File("src/test/resources/bugs/testNoSubtitle.rst");
        File out = File.createTempFile("jrst-RstToHtml2", ".html");
//        out.deleteOnExit();
        generate(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME);
    }


    /**
     * Test que s'il n'y a pas de content avant la premiere section, la section
     * est bien parsée comme tel.
     */
    @Test
    public void testNoContentBetweenTitles() throws Exception {
        File in = new File("src/test/resources/bugs/testNoContentSubtitles.rst");
        File out = File.createTempFile("jrst-testNoContentSubtitles", ".html");
//        out.deleteOnExit();
        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                Assert.assertTrue(content.indexOf("<h2>Prérequis</h2>") > 0
                        || content.indexOf("<h2 class=\"title\">Prérequis</h2>") > 0);
            }
        };
    }
}
