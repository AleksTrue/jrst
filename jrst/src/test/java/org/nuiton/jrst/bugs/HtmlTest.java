/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2015 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.bugs;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

/**
 * Test related to html specific output.
 * 
 * @author chatellier
 */
public class HtmlTest extends JRSTAbstractTest {

    /**
     * Test that custom classes is supported on various elements.
     */
    @Test
    public void testClassesSupport() throws Exception {
        File in = getBugTestFile("testCustomClasses3683.rst");
        File out = getOutputTestFile("jrst-customClasses.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            // skip simple mode
            protected void generateSimple() {
                // someone motivated can fix it in simple mode
            }

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                for (int i = 1; i <= 14; i++) {
                    Assert.assertTrue("Missing style specialformat" + i, content.contains("specialformat" + i + "\""));
                }
            }
        };
    }
    
}
