/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;

/**
 * Test concernant les directives.
 * 
 * @author chatellier
 */
public class DirectiveTest extends JRSTAbstractTest {

    /**
     * Test que les options des directives images sont correctement
     * parsées.
     */
    @Test
    public void testImageDirectiveOption() throws Exception {
        File in = getBugTestFile("testImages21.rst");
        File out = getOutputTestFile("jrst-testImages.html");
//        out.deleteOnExit();
        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                log.info(content);
                assertTrue(content.indexOf("alt=\"alternate text\"") > 0);
                assertTrue(content.indexOf("width=\"200px\"") > 0
                        || content.indexOf("width=\"200 px\"") > 0);
                assertTrue(content.indexOf("align=\"center\"") > 0);
                assertTrue(content.indexOf("alt=\"tab alternate text\"") > 0);
                // scale
                assertTrue(content.indexOf("alt=\"tab alternate text\"") > 0);
                //assertTrue(content.indexOf("width=\"49%\" height=\"49%\"") > 0);
            }
        };
    }
    
    /**
     * Test que le titre d'un content n'est pas perdu.
     * 
     * @throws Exception
     */
    @Test
    public void testContentDirective() throws Exception {
        File in = getBugTestFile("testContent877.rst");
        File out = getOutputTestFile("jrst-testContent.html");
//        out.deleteOnExit();

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("Table des matières") > 0);
            }
        };
    }
    
    /**
     * Test strong and emphasis in title.
     * 
     * @throws Exception
     */
    @Test
    public void testContentsDirective() throws Exception {
        File in = getBugTestFile("testContents.rst");
        File out = getOutputTestFile("jrst-testContents.html");
//        out.deleteOnExit();

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("<b>") > 0);
                assertTrue(content.indexOf("<em>") > 0);
            }
        };
    }
}
