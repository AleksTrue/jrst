.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Auto-symbol footnotes are also
possible, like this: [*]_ and [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [*]_ [1]_ aaaa_.

.. _aaaa: index.html
.. [1] 1
.. [*] 1
.. [*] 2
.. [*] 3
.. [*] 4
.. [*] 5
.. [*] 6
.. [*] 7
.. [*] 8
.. [*] 9
.. [*] 10
.. [*] 11
.. [*] 12
