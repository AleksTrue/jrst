.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Test missing classes support
============================

Test with litteral_block.

.. class:: specialFormat1

::

  public class Test {

  }

.. class:: specialFormat2
..

  public class Test2 {

  }

::

  public class Test3 {

  }

Test with basic paragraph.

.. class:: specialFormat3

test

.. class:: specialFormat4

Test with title
---------------

Test on subtitle.

.. role:: specialformat5

And with :specialformat5:`test`.


.. class:: specialFormat6

* listItem1

.. class:: specialFormat7

1. listItem1

   .. class:: specialFormat8
2. listItem1

.. class:: specialFormat9

- listItem1

.. class:: specialFormat10

term 1
    Definition 1.
term 2
    .. class:: specialFormat11

    Definition 2.

  .. class:: specialFormatx
term3
    Not working (even in rst2html).

.. image:: test
   :class: specialFormat12


.. image:: test
   :target: _blank
   :class: specialFormat13

.. image:: test
   :target: _blank
   :align: center
   :class: specialFormat14

