.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
===================
Test display errors
===================

Testons différents types d'erreur :

Saut de ligne manquant avant le "ou":
=====================================

JRST propose de transformer le XML de docutils avec des fichiers XSL externe.
Pour cela, il faut utiliser la commande::

  JRST -x fichierXSL, fichierXSL2 fichierRST
ou::

  JRST --xslFile fichierXSL, fichierXSL2 fichierRST


Espace manquant avant "personne" :
==================================

=======  ===============  ===============================================
Pattern  Exemple          Signification   
=======  ===============  ===============================================
*        *                Motif "joker" désignant n'importe quel élément
``//``   ``//``personne   Indique tous les descendants d'un noeud
.        .                Caractérise le noeud courant
``..``   ``..``           Désigne le noeud parent
=======  ===============  ===============================================


Erreur causées par plusieurs liens "Dossier" :
==============================================

Plan qui modifie les regles de gestion / modifying management rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sigrid V 3.3.0.8 , Plan d'analyse pour la pecherie anchois : modifie les regles
de gestion a chaque simulation `Dossier contenant tous les fichiers nécessaires à ce plan`_

.. _Dossier contenant tous les fichiers nécessaires à ce plan: ../downloads/PlanModifRules.zip

Plan qui modifie regle de gestion et parametres / modify rules and parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sigrid V 3.3.0.8 , Plan d'analyse pour la pecherie anchois : modifie les regles
de gestion, et des parametres (equations et nb de jours d'inactivite) a chaque
simulation `Dossier contenant tous les fichiers nécessaires à ce plan`_

.. _Dossier contenant tous les fichiers nécessaires à ce plan: ../downloads/PlanRulesParam.zip
