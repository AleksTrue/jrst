.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Documentation développeur
=========================

Les différentes générations
===========================

Dans cette version de JRST, deux types de génération sont proposées à l'utilisateur :

Utilisation de Jython_ et DocUtils_
-----------------------------------

La première méthode utilise DocUtils_ pour générer le document XML intermédiaire. Cependant, les scripts étant écrits en Python, il nous faut passer par Jython_ pour les lancer dans la machine virtuelle java.
Cette méthode est la plus adaptée pour la génération de documents complexes et très structurés.

Pour plus d'informations, vous pouvez visiter cette page_ .


Utilisation de notre propre parseur
-----------------------------------

La seconde méthode propose une génération plus simple et plus rapide mais celle-ci a l'inconvénient de proposer une implémentation incomplète de la spécification de ReStructuredText_.
Elle peut être utilisée dans le cadre de documents simples. L'argument pour passer dans ce mode là est "--simple".

Vous pouvez accéder à cette page la_ pour des compléments d'information.


Utilisation de XSL externe
==========================

JRST propose de transformer le XML de docutils_ avec des fichiers XSL [1]_ externe.
Pour cela, il faut utiliser la commande::

  JRST -x fichierXSL, fichierXSL2 fichierRST

ou::

  JRST --xslFile fichierXSL, fichierXSL2 fichierRST

JRST traitera le fichierRST, le XML de DocUtils_ qui est retourné sera transformé par la Class JRSTgenerator
en commençant par le fichierXSL puis par le fichierXSL2...


.. [1] Une documentation sur le XSL est diponible ici_.

.. _page: docDevDocutils.html
.. _la: docDevJRSTParser.html
.. _ici: presentationXSL.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
.. _ReStructuredText: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
