.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Documentation développeur
=========================

Le mode par défaut de génération de JRST utilise Jython_ pour exécuter les scripts de DocUtils_ pour produire le document XML intermédiaire, qui est l'implémentation de référence de la spécification_ de ReStructuredText. Cette méthode est adaptée lorsque les fichiers RST sont plus complexes ou lorsque les fonctionnalités ne sont pas encore implémentées dans notre parseur_ .

.. contents:: Sommaire

Le diagramme de Class
=====================

.. image:: ../schemas/diagrammeClassDocutils.png
   :alt: Diagramme de classes
   :align: center

La classe **JRST** contient la méthode main(), elle gère les options, la lecture et l'écriture des fichiers. Elle appelle dans un premier temps la classe **PythonInterpreter** de Jython_ permettant d'exécuter les scripts en Python de DocUtils_ pour générer un fichier XML intermédiaire. Ensuite, il ne reste qu'à appliquer le ou les XSL désiré(s) (si besoin) grâce à la classe **JRSTGenerator**.

Ce diagramme de séquence décrit le fonctionnement du parseur tout au long de la génération :

.. image:: ../schemas/diagrammeSequenceDocutils.png
   :alt: Diagramme de séquence d'une génération
   :align: center

La génération
=============

.. image:: ../schemas/diagrammeGenerationDocutils.png
   :alt: Arbre des différentes générations possibles
   :align: center

Références :

  * dn2dbk.xsl (conversion de xml de docutils vers docbook) : http://membres.lycos.fr/ebellot/dn2dbk
  * les xsl de nwalsh (conversion de docbook vers FO et xhtml) : http://nwalsh.com
  * IText (conversion de HTML vers PDF) : http://itextpdf.com/


Exemple d'utilisation
=====================


L'on souhaite convertir le document rst (text.rst) suivant en html (text.html) :

::

   =====
   Titre
   =====

   :Author: - Letellier Sylvain

   .. Attention:: texte à être réinterprété comme un fichier rst indépendant
      ceci est considéré comme un **paragraphe**

On utilise donc la commande suivante::

   JRST -t html -o text.html text.rst

La Classe **JRSTGenerator**, grâce au fichier XSL rst2xhtml.xsl, renvoie le fichier html suivant::

   <?xml version="1.0" encoding="UTF-8"?>
   <html xmlns="http://www.w3.org/TR/xhtml1/strict">
     <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
       <meta name="generator" content="JRST http://jrst.labs.libre-entreprise.org/"/>
       <title>Titre</title>
     </head>
     <body>
       <h1>Titre</h1>
       <table class="docinfo" frame="void" rules="none">
         <col class="docinfo-name"/>
         <col class="docinfo-content"/>
         <tbody valign="top">
           <tr>
             <th class="docpatterninfo-name">author :</th>
             <td class="docinfo-content">Letellier Sylvain</td>
           </tr>
         </tbody>
       </table>
       <div class="attention">
         <p class="title">attention :</p>
         <p class="body">
           <p>texte à être réinterprété comme un fichier rst indépendant
              ceci est considéré comme un <strong>paragraphe</strong></p>
         </p>
       </div>
     </body>
   </html>

Qui affiche la page (un CSS [1]_ à été ajouté pour la mise en forme) :

.. topic:: Titre

   :Author: - Letellier Sylvain
   
   .. Attention:: texte à être réinterprété comme un fichier rst indépendant
      ceci est considéré comme un **paragraphe**

.. [1] `Cascading Style Sheets`_

.. _Cascading Style Sheets: http://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade
.. _RST: http://docutils.sourceforge.net/rst.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
.. _spécification: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _parseur: docDevJRSTParser.html
