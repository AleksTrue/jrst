.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======================================================
Documentation développeur - Génération avec JRST Parser
=======================================================

Parmi les options de lancement de JRST, il est possible d'activer un mode de génération simple ( --simple ) qui utilise notre propre parseur JRST. L'avantage est de permettre une génération simplifiée et rapide qui convient parfaitement aux documents classiques.
Si jamais une fonctionnalité_ n'est pas disponible pour votre document, il est toujours possible d'utiliser l'autre mode_ .

.. contents:: Sommaire

Le diagramme de Class
=====================

.. image:: ../schemas/diagrammeClassJRSTParser.png
   :alt: Diagramme de classes
   :align: center

La Class **AdvancedReader** à pour fonction de faciliter la lecture du fichier RST_ grâce à différentes méthodes :
  - String readLine() : renvoie une ligne
  - String[] readLines(int nombresLigne) : renvoie un certain nombre de lignes
  - Stringn[] readWhile(Pattern p) : renvoie les lignes tant qu'elles correspondent au pattern
  - ...

La Class **JRSTLexer** utilise **AdvancedReader** pour construire un fichier XML, il parcours l'ensemble du document pour isoler les types de données, leurs paramètres et leurs contenus, donc rassembler toutes les informations utiles à la mise en forme du XML final. Il va commencer par l'entête du document (peekHeader(), peekDocInfo()) pour ensuite s'intéresser au corps (peekBody()).

La Class **JRSTReader** utilise **JRSTLexer**, il interprète le XML qui lui est renvoyé pour construire le XML final. Celui-ci est conforme à la DTD définie par DocUtils_. Cette Class à parfois besoin de s'appeler elle même lorsque une partie du document doit être interprétée indépendamment du reste. Par exemple, s'il y a une liste dans une case d'un tableau, l'on extrait les informations de la case et on les interprètes, le contenu d'une admoniton (une note) doit lui aussi être considéré comme un document indépendant. Lorsque la génération est terminée, la Class compose le sommaire (composeContent()) puis s'occupe de toutes les spécificités « inline » (inline()), comme par exemple les mots en italique ou gras, les références, les footnotes... Tout ce qui peut apparaître à l'intérieur d'une ligne.

La Class **reStructuredText** référence toutes les variables nécessaires à la génération du XML final.

La Class **JRST** contient la méthode main(), elle gère les options, la lecture et l'écriture des fichiers. Elle lit le document, le parse grâce à la class **JRSTReader** puis applique le XSL désiré (si besoin) grâce à la class **JRSTGenerator**.

La génération
=============

.. image:: ../schemas/diagrammeGenerationJRSTParser.png
   :alt: Arbre des différentes générations possibles
   :align: center

Références :

  * dn2dbk.xsl (conversion de xml de docutils vers docbook) : http://membres.lycos.fr/ebellot/dn2dbk
  * les xsl de nwalsh (conversion de docbook vers FO et xhtml) : http://nwalsh.com
  * IText (conversion de HTML vers PDF) : http://itextpdf.com/


Exemple d'utilisation
=====================


L'on souhaite convertir le document rst (text.rst) suivant en html (text.html) :

::

   =====
   Titre
   =====

   :Author: - Letellier Sylvain

   .. Attention:: texte à être réinterprété comme un fichier rst indépendant
      ceci est considéré comme un **paragraphe**

On utilise donc la commande suivante::

   JRST --simple -t html -o text.html text.rst

Ce diagramme de séquence décrit le fonctionnement du parseur tout au long de la génération :

.. image:: ../schemas/diagrammeSequenceJRSTParser.png
   :alt: Diagramme de séquence d'une génération
   :align: center

La Classe **JRSTGenerator**, grâce au fichier XSL rst2xhtml.xsl, renvoie le fichier html suivant::

   <?xml version="1.0" encoding="UTF-8"?>
   <html xmlns="http://www.w3.org/TR/xhtml1/strict">
     <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
       <meta name="generator" content="JRST http://jrst.labs.libre-entreprise.org/"/>
       <title>Titre</title>
     </head>
     <body>
       <h1>Titre</h1>
       <table class="docinfo" frame="void" rules="none">
         <col class="docinfo-name"/>
         <col class="docinfo-content"/>
         <tbody valign="top">
           <tr>
             <th class="docpatterninfo-name">author :</th>
             <td class="docinfo-content">Letellier Sylvain</td>
           </tr>
         </tbody>
       </table>
       <div class="attention">
         <p class="title">attention :</p>
         <p class="body">
           <p>texte à être réinterprété comme un fichier rst indépendant
              ceci est considéré comme un <strong>paragraphe</strong></p>
         </p>
       </div>
     </body>
   </html>

Qui affiche la page (un CSS [1]_ à été ajouté pour la mise en forme) :

.. topic:: Titre

   :Author: - Letellier Sylvain
   
   .. Attention:: texte à être réinterprété comme un fichier rst indépendant
      ceci est considéré comme un **paragraphe**

.. [1] `Cascading Style Sheets`_

.. _Cascading Style Sheets: http://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade
.. _RST: http://docutils.sourceforge.net/rst.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _fonctionnalité: ../user/fonctionnalites.html
.. _mode: docDevDocutils.html
