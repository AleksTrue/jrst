.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

====================================
Les différentes librairies utilisées
====================================

.. contents:: Sommaire

dom4j_
======

Dom4j_ est une API Open Source Java permettant de travailler avec XML, XPath et XSLT. Cette bibliothèque est compatible avec les standards DOM, SAX et JAXP.

Saxon_
======

Saxon_ est une bibliothèque permettant d'effectuer des traitements XSLT. Dans ce projet, elle permet d'appliquer les feuilles de style XSL aux documents XML.

IText_
======

IText_ est une bibliothèque permettant de générer des documents PDF à partir de pages (x)HTML.

Xmlunit_
========

Xmlunit_ permet de comparer deux fichiers XML pour mettre en évidences les différences.

DocUtils_
=========

DocUtils_ est un ensemble de scripts en Python permettant de transformer des fichiers RST_ en formats plus répendus comme le HTML, ODT, Latex et bien plus encore.

Jython_
=======

Jython_ est un interprète Python en Java. Ici, il permet d'exécuter les scripts de DocUtils_.

.. _dom4j: http://www.dom4j.org
.. _javax.xml.transform: http://java.sun.com/javase/6/docs/api/javax/xml/transform/package-summary.html
.. _IText: http://itextpdf.com/
.. _Xmlunit: http://xmlunit.sourceforge.net/
.. _RST: http://docutils.sourceforge.net/rst.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
