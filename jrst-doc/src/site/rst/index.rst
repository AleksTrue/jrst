.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===================================
Parseur reStructuredText_ : le JRst
===================================

.. contents:: Sommaire

Documentation utilisateur
=========================

Présentation
------------

Le format reStructuredText_ est un format de description de documents. A l'image
d'autres LaTeX_ ou DocBook_, il peut être décliné en une multitude de formats. Ces
formats souffrent habituellement d'une syntaxe envahissante qui, si elle est
nécessaire pour des documents très spécifiques, devient gênante quand il s'agit
de créer rapidement un document pas trop complexe. RST_ dispose quant à lui d'une
syntaxe tellement simple qu'elle en devient presque invisible.

Deux méthodes de génération sont proposées :
    - La première utilise Jython_ pour lancer des scripts de DocUtils_ afin de transformer le RST_ en XML. Toutes les fonctionnalités proposées par ReStructuredText sont prises en comptes mais la génération est un peu plus longue puisque celle-ci est basée sur les scripts externes de DocUtils.
    - La seconde utilise notre propre parseur JRST pour générer ce XML, ce qui permet d'obtenir de très bonne performances mais certaines fonctionnalités_ ne sont pas encore implémentées.

A partir du document XML, nous permettons la transformation en divers formats comme HTML ou PDF.

Usage
-----

JRST prend un fichier reStructuredText_ en entrée et génère un fichier XML
qui pourra ensuite servir à produire divers formats de fichiers grâce à des fichiers
XSL de générations. Les formats de sortie disponibles sont le html, le xhtml, le rst,
le pdf, le docbook_ ou du XML [1]_.

Lorsque JRST est lancé sans argument, une interface graphique est proposée à l'utilisateur. Ainsi, on peut choisir plus aisément les fichiers, le type de sortie, les feuilles de style XSL externes à appliquer ou bien la génération simplifiée ou non.

::

   JRST myfile.rst

Cette commande aura pour effet de convertir le fichier myfile.rst en XML qui sera affiché sur la sortie standard (console).
Plusieurs options sont disponibles :

-o file, --outFile=file          Pour rediriger la sortie vers un fichier.
-t format, --outType format      Pour préciser un format de sortie, donc utiliser un ou des fichiers XSL_ de génération. Plusieurs formats sont disponibles xhtml, docbook, xml, html, xdoc, pdf.
-x xslFile, --xslFile xslFile    Sert à préciser le fichier xsl de génération à utiliser.
--force                          Forcer l'écriture d'un fichier, si le fichier de sortie existe, il sera remplacé.
--simple                         Utiliser notre propre parseur JRST pour générer le XML intermédiaire. Toutes les fonctionnalités ne sont pas implémentées mais la génération est plus rapide.
--help                           Pour afficher les options disponibles :


::

   Usage: [options] FILE
      [--force] : overwrite existing out file
      [--simple] : use our own parser to generate the file
      [--help] : display this help and exit
      [--outFile -o value] : Output file
      [--outType -t /xhtml|docbook|xml|html|xdoc|rst/] : Output type
      [--xslFile -x value] : XSL file list to apply, comma separated


ex :

::

   JRST --force -t html -o myfile.html myfile.rst

Cette commande produira un fichier html (myfile.html) à partir du fichier reStructuredText_ (myfile.rst)
même si myfile.html existe déjà.


Plugin Maven_
-------------

Un plugin Maven_ est disponible à l'adresse suivante 
http://maven-site.nuiton.org/jrst/maven-jrst-plugin. Il permet l'utilisation 
depuis Maven_ de JRst.

.. [1] Seuls les formats html, xhtml, DocBook_, xdoc et pdf sont disponibles pour le moment.

.. _reStructuredText: ./user/presentationRST.html
.. _RST: ./user/presentationRST.html
.. _Maven: http://maven.apache.org/
.. _XSL: ./devel/presentationXSL.html
.. _DocBook: http://www.docbook.org/
.. _LaTex: http://www.latex-project.org/
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
.. _fonctionnalités: ./user/fonctionnalites.html
