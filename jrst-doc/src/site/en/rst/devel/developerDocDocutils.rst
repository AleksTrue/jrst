.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======================
Developer Documentation
=======================

Default mode for the JRST generation uses Jython_ and DocUtils_ to produce the intermediate XML document, which is the reference implementation of the ReStructuredText specification_. This method is usefull when RST file are complex or if the other method with `our parser`_ doesn't work.

.. contents::

Class diagram
=============

.. image:: ../../schemas/diagrammeClassDocutils.png
   :align: center

**JRST** Class contents main() method, it's looking after options, files reading and writing. Initially, it calls Jython_ class **PythonInterpreter** which allows to execute DocUtils' Python script to generate an intermediate file. Then it only remains to apply several desired XSL (if needed) by using **JRSTGenerator** Class.

This sequence diagram describes the parser's mechanism throughout the generation :

.. image:: ../../schemas/diagrammeSequenceDocutils.png
   :align: center

Generation
==========

.. image:: ../../schemas/diagrammeGenerationDocutils.png
   :align: center

Links :

  * dn2dbk.xsl (conversion from xml of docutils towards docbook) : http://membres.lycos.fr/ebellot/dn2dbk
  * nwalsh xsl (conversion from docbook towards FO and xhtml) : http://nwalsh.com
  * IText (conversion from HTML towards PDF) : http://itextpdf.com/

Use example
===========

We want to convert this following reStructuredText document (text.rst) to html (text.html) :

::

   =====
   Title
   =====
   
   :Author: - Letellier Sylvain
   
   .. Attention:: this text must be interpreted independently
      because it must be interpreted like a **paragraph**


So we use this following command::

   JRST -t html -o text.html text.rst

**JRSTGenerator** Class with the XSL file rst2xhtml.xsl, returns following html file::

   <?xml version="1.0" encoding="UTF-8"?>
   <html xmlns="http://www.w3.org/TR/xhtml1/strict">
     <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
       <meta name="generator" content="JRST http://jrst.labs.libre-entreprise.org/"/>
       <title>Title</title>
     </head>
     <body>
       <h1>Title</h1>
       <table class="docinfo" frame="void" rules="none">
         <col class="docinfo-name"/>
         <col class="docinfo-content"/>
         <tbody valign="top">
           <tr>
             <th class="docpatterninfo-name">author :</th>
             <td class="docinfo-content">Letellier Sylvain</td>
           </tr>
         </tbody>
       </table>
       <div class="attention">
         <p class="title">attention :</p>
         <p class="body">
           <p>this text must be interpreted independently
              because it must be interpreted like a <strong>paragraph</strong>
           </p>
         </p>
       </div>
     </body>
   </html>

What display the page (CSS [1]_ was added to layout) :

.. topic:: Titre

   :Author: - Letellier Sylvain

   .. Attention:: this text must be interpreted independently
      because it must be interpreted like a **paragraph**

.. [1] `Cascading Style Sheets`_

.. _here: presentationXSL.html
.. _`our parser`: developerDocJRSTParser.html
.. _specification: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _RST: http://docutils.sourceforge.net/rst.html
.. _Cascading Style Sheets: http://en.wikipedia.org/wiki/Cascading_Style_Sheets
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
