.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======================
Developer Documentation
=======================

Types of generation
===================

In this version of JRST, the user can choose two types of generation :

Use of Jython_ and DocUtils_
----------------------------

The first method uses DocUtils_ to generate intermediate XML document. However, scripts are written in Python, so we have to use Jython_ to launch them in the java virtual machine.
This is the best way if RST documents are complex and structured.

For further information, please visit this page_ .

Use of our own parser
---------------------

The second method allows a simpler and faster generation but the whole ReStructuredText specification_ is not available. It could be used with simple documents. To use this mode, just add the argument "--simple".

You can visit this page there_ if you want more informations.

Uses external XSL
=================

JRST able to transform DocUtils XML with external XSL [1]_ files.
You must use this following command::

  JRST -x XSLfile, XSLfile2 RSTfile

or::

  JRST --xslFile XSLfile, XSLfile2 RSTfile

JRST process RST_ file, returned DocUtils_ XML will be transformed by JRSTgenerator Class
starting by XSLfile then XSL2file2...

.. [1] XSL documentation is available here_.

.. _here: presentationXSL.html
.. _page: developerDocDocutils.html
.. _there: developerDocJRSTParser.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
.. _specification: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _RST: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
